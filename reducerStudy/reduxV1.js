//a really simple version of using redux to store and update the state
import redux from "redux";
const createStore = redux.createStore;

function createReducer(initialState, handlers) {
  return function reducer(state = initialState, action) {
    if (action.type in handlers) {
      return handlers[action.type](state, action);
    } else {
      console.log("missed the property", action, handlers);
      return state;
    }
  };
}

const initialState = {
  age: 10,
  balance: 0,
};

class SimpleHandler {
  birthday(state) {
    return { ...state, age: state.age + 1 };
  }

  deposit(state, action) {
    return { ...state, balance: state.balance + action.amount };
  }

  withdraw(state, action) {
    return { ...state, balance: state.balance - action.amount };
  }
}

const objHandler = new Object();
objHandler.birthday = (state) => ({ ...state, age: state.age + 1 });
objHandler.deposit = (state, action) => ({
  ...state,
  balance: state.balance + action.amount,
});
objHandler.withdraw = (state, action) => ({
  ...state,
  balance: state.balance - action.amount,
});

console.log("initializing the store");
const store = createStore(createReducer(initialState, objHandler));
console.log("the store is", store);
console.log("the state is", store.getState());

// now let's dispatch some actions by hand
console.log("dispatching birthday");
store.dispatch({ type: "birthday" });
console.log("birthday result is", store.getState());

console.log("dispatch deposit");
store.dispatch({ type: "deposit", amount: 2000 });
console.log("deposit result is", store.getState());

console.log("dispatch withdraw");
store.dispatch({ type: "withdraw", amount: 1000 });
console.log("withdraw result is", store.getState());

const handler = new SimpleHandler();
console.log("handler is", handler);
console.log("handler test, birthday", handler.birthday({ age: 1 }, {}));
console.log("handler has birthday", handler.hasOwnProperty("birthday"));
console.log("birthday in handler", "birthday" in handler);
console.log("handler.birthday is", handler.birthday);
