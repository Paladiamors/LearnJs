//using a more proper method of constants to define the action name and adding the dispatch
import redux from "redux";
const createStore = redux.createStore;
const combineReducers = redux.combineReducers

function createReducer(initialState, handlers) {
  return function reducer(state = initialState, action) {
    if (action.type in handlers) {
      return handlers[action.type](state, action);
    } else {
      return state;
    }
  };
}

const initialState = {
  name: null,
  age: 10,
  balance: 0,
};

const actions = {
  deposit: "account/deposit",
  withdraw: "account/withdraw",
  birthday: "account/birthday",
};

function withdraw(state, action) {
  return { ...state, balance: state.balance - action.amount };
}

function deposit(state, action) {
  return { ...state, balance: state.balance + action.amount };
}

function birthday(state) {
  return { ...state, age: state.age + 1 };
}

const accoutnHandler = {};
accoutnHandler[actions.deposit] = deposit;
accoutnHandler[actions.withdraw] = withdraw;
accoutnHandler[actions.birthday] = birthday;

function dispatchBirthday() {
  return { type: actions.birthday };
}

function dispatchDeposit(amount) {
  return { type: actions.deposit, amount };
}

function dispatchWithdraw(amount) {
  return { type: actions.withdraw, amount };
}

// running a test of the store
const store = createStore(createReducer(initialState, accoutnHandler));

store.dispatch(dispatchBirthday());
console.log("dispatched birthday state", store.getState());

store.dispatch(dispatchDeposit(2000));
console.log("dispatched deposit state", store.getState());

store.dispatch(dispatchWithdraw(1000));
console.log("dispatched withdraw state", store.getState());