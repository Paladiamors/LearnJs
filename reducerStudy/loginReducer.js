// node seems to need the .js at the end of the file for the import to work...
import { createReducer } from "./utils.js";

const actions = {
  login: "auth/login",
  logout: "auth/logout",
};

const initialState = {
  loggedIn: false,
};

function login(state, action) {
  return { ...state, loggedIn: true };
}

function logout(state, action) {
  return { ...state, loggedIn: false };
}

//defining dispatches

export function dispatchLogin() {
  return { type: actions.login };
}

export function dispatchLogout() {
  return { type: actions.logout };
}

const loginHandler = {};
loginHandler[actions.login] = login;
loginHandler[actions.logout] = logout;

export const loginReducer = createReducer(initialState, loginHandler)
