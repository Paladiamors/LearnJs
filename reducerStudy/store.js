import redux from "redux";
import { accountReducer, dispatchBirthday, dispatchDeposit } from "./accountReducer.js";
import { loginReducer, dispatchLogin } from "./loginReducer.js";
const createStore = redux.createStore;
const combineReducers = redux.combineReducers;

const store = createStore(
  combineReducers({
    account: accountReducer,
    auth: loginReducer,
  })
);

console.log(store.getState())
store.dispatch(dispatchLogin())
console.log("state after logging in", store.getState())
store.dispatch(dispatchBirthday())
store.dispatch(dispatchDeposit(2000))
console.log("after birthday deposit", store.getState())
