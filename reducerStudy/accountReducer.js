//using a more proper method of constants to define the action name and adding the dispatch
import redux from "redux";
const createStore = redux.createStore;

function createReducer(initialState, handlers) {
  return function reducer(state = initialState, action) {
    if (action.type in handlers) {
      return handlers[action.type](state, action);
    } else {
      return state;
    }
  };
}

const initialState = {
  name: null,
  age: 10,
  balance: 0,
};

const actions = {
  deposit: "account/deposit",
  withdraw: "account/withdraw",
  birthday: "account/birthday",
};

function withdraw(state, action) {
  return { ...state, balance: state.balance - action.amount };
}

function deposit(state, action) {
  return { ...state, balance: state.balance + action.amount };
}

function birthday(state) {
  return { ...state, age: state.age + 1 };
}

const accountHandler = {};
accountHandler[actions.deposit] = deposit;
accountHandler[actions.withdraw] = withdraw;
accountHandler[actions.birthday] = birthday;

export const accountReducer = createReducer(initialState, accountHandler);

export function dispatchBirthday() {
  return { type: actions.birthday };
}

export function dispatchDeposit(amount) {
  return { type: actions.deposit, amount };
}

export function dispatchWithdraw(amount) {
  return { type: actions.withdraw, amount };
}
