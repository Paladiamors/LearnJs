export function createReducer(initialState, handlers) {
    return function reducer(state = initialState, action) {
      if (action.type in handlers) {
        return handlers[action.type](state, action);
      } else {
        return state;
      }
    };
  }