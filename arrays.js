function forEachLoop() {
  const arr = [...Array(5).keys()];

  console.log("the array is", arr);
  arr.forEach((x) => {
    console.log("value", x);
  });
}

function forLoop() {
  var i;
  for (i = 0; i < 5; i++) {
    console.log("for loop i", i);
  }
}

function whileLoop() {
  var i = 0;
  while (i < 5) {
    console.log("while loop i", i);
    i++;
    if (i == 3) {
      console.log("i is 3!");
      break;
    }
  }
}

// forEachLoop();
// forLoop();
// whileLoop();

const people = ["bob", "jones", "alice", "alex", "paul"];
const persons = ["alica", "kerry", ...people]

console.log("bob in people", "bob" in people);
console.log("bob included people", people.includes("bob"))
console.log("index of bob", people.indexOf("bob"))
console.log("slicing people", people.slice(1))
console.log("slicing people", people.slice(-2))
console.log("negative index", people[-1]) // interesting negative index does not work

console.log("extending people with persons", persons)
