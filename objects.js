const data = { height: 20, width: 100, name: "table" };
const extendedData = { ...data, price: 100 };
const overrideData = { ...data, width: 200, price: 300 };

console.log("data is", data);
console.log("extendedData is", extendedData);
console.log("overrideData is", overrideData);
